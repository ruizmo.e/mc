from ventana_ui import *
import utils
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QGridLayout, QWidget
from PyQt5.QtGui import QIcon, QPixmap
import matplotlib.pyplot as plt

class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self, *args, **kwargs):
        QtWidgets.QMainWindow.__init__(self, *args, **kwargs)
        self.setupUi(self)

        self.button_01.clicked.connect(self.test_01_button)
        self.button_01.setToolTip("Si el resultado es ~=1, entonces será un sistema caótico, de ser ~=0 se trataría de un sistema estable")
        self.button_lyapunov.setCheckable(True)
        self.button_lyapunov.setChecked(False)
        self.button_01.setCheckable(True)
        self.button_01.setChecked(False)
        self.button_lyapunov.setToolTip("Si el resultado es >0, entonces será un sistema caótico, de lo contrario se trataría de un sistema estable" )
        self.label_descripcion_1.setText( "Introduzca la funcion\n(se permite el uso de símbolos 'r' y 'x' para definirla):")
        self.button_map.setDisabled(True)
        self.button_lyapunov.clicked.connect(self.test_lyapunov_button)
        self.line_funcion.textChanged.connect(self.check_text_change)
        self.slider_r.valueChanged.connect(self.slider_change)
        self.button_feigenbaum.clicked.connect(self.plot_feigenbaum)
        self.button_map.clicked.connect(self.plot_map)
        self.line_funcion.setPlaceholderText("Usando r*x*(1-x) por defecto")

        self.funcion = None
        self.r = 3.55
        self.slider_r.setValue(int(self.r * 10))
        self.activo = None

    def plot_feigenbaum(self):
        utils.bifurcacion_logistica()
        self.label_grafica.setPixmap(QPixmap('foo.png'))

    def plot_map(self):
        self.label_grafica.setPixmap(QPixmap())
        if self.button_lyapunov.isChecked():
            print("Plot de lyapunov")
            if self.funcion:
                print(str(self.slider_r.value()))
                utils.lyapunov_plot(fun=self.funcion, r_to_plot=self.r)
            else:
                utils.lyapunov_logistica_plot(r_to_plot=self.r)
            print("Representando...")
            self.label_grafica.setPixmap(QPixmap('foo.png'))
        elif self.button_01.isChecked():
            print("Plot de test 0 - 1")
            utils.searchK(r=self.r, f=self.funcion)
            print("Representando...")
            self.label_grafica.setPixmap(QPixmap('foo.png'))
        else:
            pass

    def check_text_change(self):
        self.button_map.setDisabled(True)
        if any(e not in "( ) + - * / ^ r x 0 1 2 3 4 5 6 7 8 9".split(' ') for e in self.line_funcion.text()):
            self.label_resultado.setText('Atención la función introducida contiene caracteres no válidos,\nutilice: (, ), +, -, *, /, ^, r, x, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9.')
            self.button_01.setDisabled(True)
            self.button_lyapunov.setDisabled(True)
            self.button_feigenbaum.setDisabled(True)
        else:
            if self.line_funcion.text() == "r*x*(1-x)":
                self.button_feigenbaum.setDisabled(False)
            else:
                self.button_feigenbaum.setDisabled(True)
            if not(self.button_01.isEnabled()):
                self.button_01.setDisabled(False)
                self.button_lyapunov.setDisabled(False)
                self.label_resultado.setText("")
                if not self.funcion:
                    self.funcion = "r*x*(1-x)"
                else:
                    self.funcion = self.line_funcion.text()

    def slider_change(self):
        self.r = int(self.slider_r.value())/10
        self.label_r.setText(str(self.r))

    def test_01_button(self):
        self.button_01.setStyleSheet('background-color:#DCDCDC;border: 2px solid #222222')
        self.button_lyapunov.setStyleSheet('background-color:#DCDCDC;border: 1px solid #A9A9A9')
        self.label_grafica.setPixmap(QPixmap())
        self.button_map.setDisabled(False)
        self.button_01.setChecked(True)
        self.button_lyapunov.setChecked(False)


        k= utils.searchK(r=self.r,f=self.funcion)
        self.label_resultado.setText('Resultado del test 0 - 1: ' + str(k))

    def test_lyapunov_button(self):
        self.button_lyapunov.setStyleSheet('background-color:#DCDCDC;border: 2px solid #222222')
        self.button_01.setStyleSheet('background-color:#DCDCDC;border: 1px solid #A9A9A9')
        self.label_grafica.setPixmap(QPixmap())
        self.button_map.setDisabled(False)
        self.button_01.setChecked(False)
        if self.line_funcion.text():
            self.funcion = self.line_funcion.text()
        if self.funcion:
            output = utils.lyapunov(r=self.r, f=self.funcion)
        else:
            output = utils.lyapunov_logistica(r=self.r)
        self.label_resultado.setText('Resultado del test de Lyapunov: ' + str(output))


if __name__ == "__main__":
    app = QtWidgets.QApplication([])
    window = MainWindow()
    window.show()
    app.exec_()